import {SizeType} from './sizeType.model';

export class SubCategory {

  constructor(
    public id: number,
    public name: string,
    public sizeType?: SizeType
  ) {}

}
