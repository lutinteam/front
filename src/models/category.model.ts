import {SizeType} from './sizeType.model';
import {SubCategory} from './subCategory.model';

export class Category {

  constructor(
    public id: number,
    public name: string,
    public subCategories?: SubCategory[],
  ) {}

}
