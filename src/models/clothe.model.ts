import {Color} from './color.model';
import {Size} from './size.model';
import {Category} from './category.model';
import {SubCategory} from './subCategory.model';
import {Photo} from './photo.model';
import {Pattern} from './pattern.model';

export class Clothe {

  constructor(
    public id: number,
    public firstColor: Color,
    public category: Category,
    public subCategory: SubCategory,
    public size: Size,
    public brand: string,
    public secondColor: Color,
    public pattern: Pattern,
    public description: string,
    public photos: Photo[]
  ) {
  }

}
