import {SizeType} from './sizeType.model';

export class Size {

  constructor(
    public id: number,
    public size: string,
    public sizeType: SizeType,
  ) {}

}
