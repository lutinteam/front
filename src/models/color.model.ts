export class Color {

  constructor(
    public id: number,
    public name: string,
    public hexValue?: string,
  ) {}

}
