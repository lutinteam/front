import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {SideMenuComponent} from './side-menu/side-menu.component';
import {ContentComponent} from './content/content.component';
import {NewClotheComponent} from './content/new-clothe/new-clothe.component';
import {ClotheService} from './services/clothe.service';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {RouterModule} from '@angular/router';
import {SuiModule} from 'ng2-semantic-ui';
import {DROPZONE_CONFIG, DropzoneConfigInterface, DropzoneModule} from 'ngx-dropzone-wrapper';
import { HeaderComponent } from './header/header.component';


const appRoutes = [
  {path: 'clothes', component: ContentComponent},
  {path: 'clothes/new', component: NewClotheComponent},
  {path: 'clothes/:id/update', component: NewClotheComponent},
  {path: '', redirectTo: 'clothes', pathMatch: 'full'}
];

const DEFAULT_DROPZONE_CONFIG: DropzoneConfigInterface = {
  // Change this to your upload POST address:
  url: 'http://localhost:8080/Dressing/api/uploadFile',
  maxFilesize: 50,
  acceptedFiles: 'image/*'
};

@NgModule({
  declarations: [
    AppComponent,
    SideMenuComponent,
    ContentComponent,
    NewClotheComponent,
    HeaderComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    SuiModule,
    DropzoneModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [
    ClotheService,
    {
      provide: DROPZONE_CONFIG,
      useValue: DEFAULT_DROPZONE_CONFIG
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
