import {Component, OnInit} from '@angular/core';
import {ClotheService} from '../../services/clothe.service';
import {SubCategory} from '../../../models/subCategory.model';
import {Size} from '../../../models/size.model';
import {Color} from '../../../models/color.model';
import {Category} from '../../../models/category.model';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {Clothe} from '../../../models/clothe.model';
import {Pattern} from '../../../models/pattern.model';
import {DropzoneConfigInterface} from 'ngx-dropzone-wrapper';
import {UploadImagesService} from '../../services/upload-images.service';
import {Photo} from '../../../models/photo.model';

declare var $: any;

@Component({
  selector: 'app-new-clothe',
  templateUrl: './new-clothe.component.html',
  styleUrls: ['./new-clothe.component.css']
})
export class NewClotheComponent implements OnInit {

  clothe: Clothe;

  category: Category;
  categories: Category[];

  size: Size;
  sizes: Size[];
  sizesFiltered: any[];

  colors: Color[];
  colorsChoosen: Color[];

  subCategories: SubCategory[];
  subCategory: SubCategory;

  pattern: Pattern;
  patterns: Pattern[];

  brands: any[];

  photos: Photo[] = [];

  saved = false;
  saving = false;
  update = false;
  myDropzone;

  newClotheForm: FormGroup;

  public config: DropzoneConfigInterface = {
    clickable: true,
    maxFiles: 4,
    autoReset: null,
    errorReset: null,
    cancelReset: null,
    createImageThumbnails: true,
    thumbnailWidth: 200,
    thumbnailHeight: 200,
    addRemoveLinks: true
  };

  constructor(private clotheService: ClotheService,
              private uploadImagesService: UploadImagesService,
              private formBuilder: FormBuilder,
              private router: Router,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.clotheService.getNewClothForm();
    const id = this.route.snapshot.params['id'];

    this.initForm();

    this.brands = this.clotheService.brands;

      // UPDATE CLOTHE FORM
    if (id) {
      this.update = true;
      this.clotheService.getUpdateClothForm(+id).pipe().subscribe(
        (datas) => {
          $('.select-category').removeClass('loading');
          $('.select-color').removeClass('loading');
          this.categories = datas[0].categories.sort((a, b) => a.name > b.name ? 1 : -1);
          this.sizes = datas[0].sizes;
          this.colors = datas[0].colors;
          this.patterns = datas[0].patterns;
          this.clothe = datas[1];
          this.category = this.categories.find(category => category.id === this.clothe.category.id);
          this.subCategories = this.category.subCategories.sort((a, b) => a.name > b.name ? 1 : -1);
          this.subCategory = this.category.subCategories.find(subCategory => this.clothe.subCategory.id === subCategory.id);
          this.sizesFiltered = this.sizes.filter(v => v.sizeType.id === this.subCategory.sizeType.id)
                                          .sort((a, b) => {
                                            return a.size.substr(a.size.length - 2) > b.size.substr(b.size.length - 2) ? 1 : -1;
                                          });
          this.size = this.sizesFiltered.find(size => this.clothe.size.id === size.id);

          this.colorsChoosen = this.colors.filter(color => {
            if (this.clothe.secondColor) {
              return this.clothe.firstColor.id === color.id || this.clothe.secondColor.id === color.id;
            } else {
              return this.clothe.firstColor.id === color.id;
            }
          });

          this.pattern = this.clothe.pattern ? this.patterns.find(pattern => pattern.id === this.clothe.pattern.id) : null;

          this.newClotheForm.patchValue(
            {
              id: this.clothe.id,
              brand: this.clothe.brand,
              category: this.category,
              subCategory: this.subCategory,
              size: this.size,
              pattern: this.pattern,
              description: this.clothe.description,
              colors: this.colorsChoosen
            }
          );

        this.update = false;
        }
      );
      // CREATE CLOTHE FORM
    } else {
      this.clotheService.getNewClothForm().pipe().subscribe(
        (datas) => {
          $('.select-category').removeClass('loading');
          $('.select-color').removeClass('loading');
          // @ts-ignore
          this.categories = datas.categories.sort((a, b) => a.name > b.name ? 1 : -1);
          // @ts-ignore
          this.sizes = datas.sizes;
          // @ts-ignore
          this.colors = datas.colors;
          // @ts-ignore
          this.patterns = datas.patterns;
        }
      );
    }

    $('.ui.dropdown').dropdown();
    $('.ui.accordion').accordion();

    $('.ui.search')
      .search({
        source: this.brands,
        fields: {
          title: 'name'
        },
        searchFields: [
          'name'
        ],
        fullTextSearch: false,
        onSelect: (result) => {
          this.newClotheForm.controls['brand'].setValue(result.name);
        }
      });

    $('.ui.selection.color.dropdown')
      .dropdown({
        maxSelections: 2
      });

    $('.ui.selection.pattern.dropdown')
      .dropdown({
        clearable: true
      });

    $('.select-category').addClass('loading');
    $('.select-color').addClass('loading');
  }

  /**
   * Called when user select a Category
   */
  onSelectCategory() {
    if (!this.update) {
      const formValue = this.newClotheForm.value;
      this.category = formValue['category'];

      this.subCategories = this.category.subCategories.sort((a, b) => a.name > b.name ? 1 : -1);
      this.newClotheForm.controls['subCategory'].setValue(this.subCategories[0]);
      this.subCategory = this.newClotheForm.controls['subCategory'].value;

      this.sizesFiltered = this.sizes.filter(v => v.sizeType.id === this.subCategory.sizeType.id)
        .sort((a, b) => a.size.substr(a.size.length - 2) > b.size.substr(b.size.length - 2) ? 1 : -1);
      this.newClotheForm.controls['size'].setValue(this.sizesFiltered[0]);
    }
  }

  /**
   * Initialize Form to null whith validators
   */
  private initForm() {
    this.newClotheForm = this.formBuilder.group(
      {
        id: [null],
        category: [null, Validators.required],
        subCategory: [null, Validators.required],
        size: [null, Validators.required],
        brand: [null],
        colors: [null, Validators.required],
        pattern: [null],
        description: [null]
      }
    );
  }

  /**
   * Called when user click on 'Enregistrer'
   */
  onSaveClothe() {
    this.saving = true;
    const formValue = this.newClotheForm.value;
    const clothe = new Clothe(
      formValue['id'],
      formValue['colors'][0],
      formValue['category'],
      formValue['subCategory'],
      formValue['size'],
      formValue['brand'],
      formValue['colors'][1],
      formValue['pattern'],
      formValue['description'],
      this.photos
    );
    console.log(clothe);
    this.clotheService.saveNewClothe(clothe).then(
      () => {
        this.saved = true;
        this.router.navigate(['/clothes']);
      }
    );
  }

  /**
   * Called when user click on 'Annuler'
   */
  onCancel() {
    this.router.navigate(['/clothes']);
  }

  public onUploadInit(args: any): void {
    console.log('onUploadInit:', args);
  }

  public onUploadError(args: any): void {
    console.log('onUploadError:', args);
  }

  public onUploadSuccess(args: any) {
    this.photos.push(args[1]);
  }
}

