import {AfterViewInit, Component, DoCheck, OnDestroy, OnInit} from '@angular/core';
import {ClotheService} from '../services/clothe.service';
import {Router} from '@angular/router';
import {Clothe} from '../../models/clothe.model';
import {Subscription} from 'rxjs';

declare var $: any;

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.css']
})
export class ContentComponent implements OnInit, OnDestroy, AfterViewInit {

  clothes: Clothe[];
  clothesSubscription: Subscription;

  constructor(private clotheService: ClotheService,
              private router: Router) { }

  ngOnInit() {
    this.clothesSubscription = this.clotheService.clothesSubject.subscribe(
      (clothes: Clothe[]) => {
        this.clothes = clothes;
      }
    );

    this.clotheService.getAllClothes().then(
      () => {
        this.clotheService.emitClothes();
        $('.image').dimmer({
          on: 'hover'
        });
      }
    );
    // TODO tester sans 'emit'
  }

  ngOnDestroy(): void {
    this.clothesSubscription.unsubscribe();
    $('.ui.modal.hidden').remove();
  }

  onSelectClothe(index: number, id: number) {
    $('.ui.modal.select.' + index)
      .modal('show');

    $('.ui.modal.validate')
      .modal({
        onDeny: () => true,
        onApprove: () => {
          const clothe = this.clothes.find((c: Clothe) => c.id === id);
          if (clothe) { this.clotheService.deleteClothe(clothe); }
        }
      })
      .modal('attach events', '.ui.modal.select .ui.deny');
  }

  onUpdate(id) {
    this.router.navigate(['/clothes', id, 'update']);
  }

  ngAfterViewInit(): void {
    $('.ui.modal.hidden').remove();
  }
}
