import {Injectable} from '@angular/core';

import {HttpClient} from '@angular/common/http';
import {forkJoin, Subject} from 'rxjs';
import {Clothe} from '../../models/clothe.model';
import {Observable} from 'rxjs';


@Injectable()
export class ClotheService {

  private clothes: Clothe[] = [];
  clothesSubject = new Subject<Clothe[]>();

  brands: string[] = [];

  constructor(private httpClient: HttpClient) { }

  /**
   * send clothes to listener
   */
  emitClothes() {
    this.clothesSubject.next(this.clothes);
  }

  /**
   * save clothe on server
   * @param clothe
   */
  saveNewClothe(clothe: Clothe) {
    return new Promise(
      (resolve, reject) => {
        this.httpClient
          .post('http://localhost:8080/Dressing/api/clothes', clothe)
          .subscribe(
            () => {
              resolve(true);
              console.log('Enregistrement terminé !');
            },
            (error) => {
              console.log('Erreur : ' + JSON.stringify(error));
              reject();
            }
          );
      });
  }

  /**
   * return Form datas from server
   */
  getNewClothForm(): Observable<any[]> {
    return this.httpClient
      .get<any>('http://localhost:8080/Dressing/api/formsdatas/clothes');
  }

  /**
   * Return Form datas and clothe data for id only when both are done.
   * @param id
   */
  getUpdateClothForm(id: number): Observable<any> {
    return forkJoin(
      this.getNewClothForm(),
      this.getClothe(id),
    );
  }

  /**
   * return clothe data for id
   * @param id
   */
  getClothe(id: number): Observable<Clothe> {
    return this.httpClient
      .get<Clothe>(`http://localhost:8080/Dressing/api/clothes/${id}`);
  }

  /**
   * return all clothes from server
   */
  getAllClothes() {
    return new Promise(
      ((resolve, reject) => {
        this.httpClient
          .get<Clothe[]>('http://localhost:8080/Dressing/api/clothes')
          .subscribe(
            (data) => {
              this.clothes = data;
              this.emitClothes();
              console.log(this.clothes);
              resolve();
            },
            (error) => {
              console.log('Erreur : ' + JSON.stringify(error));
              reject();
            }
          );
      })
    );
  }

  /**
   * Delete clothe from server
   * @param clothe
   */
  deleteClothe(clothe: Clothe) {
    this.httpClient
      .delete(`http://localhost:8080/Dressing/api/clothes/${clothe.id}`)
      .subscribe(
        () => {
          console.log('Supression terminé !');
          const index = this.clothes.findIndex((el: Clothe) => el.id === clothe.id);
          this.clothes.splice(index, 1);
          this.emitClothes();
        },
        (error) => {
          console.log('Erreur : ' + JSON.stringify(error));
        }
      );
  }
}
